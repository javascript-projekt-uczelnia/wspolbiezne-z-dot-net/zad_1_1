﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

class Program
{
	private static int pointsInCircle = 0;
	private static readonly object lockObject = new object();

	static void Main(string[] args)
	{
		const string PI = "3.141592653589793238462643383279502884197";
		int threadsCount = Environment.ProcessorCount;
		const int pointsPerThread = 1000000;

		Console.WriteLine("Wartość Pi: " + PI);
		Console.WriteLine("Wykorzystane wątki: " + threadsCount);
		Console.WriteLine("Wygenerowano punktów na wątek: " + pointsPerThread + "\n");

		CalculateWithThread(threadsCount, pointsPerThread);
		CalculateWithThreadPool(threadsCount, pointsPerThread);
	}

	static void MonteCarloPi(int points)
	{
		int localCount = 0;
		Random random = new Random();

		for (int i = 0; i < points; ++i)
		{
			double x = random.NextDouble();
			double y = random.NextDouble();

			if (x * x + y * y <= 1.0)
				localCount++;
		}

		lock (lockObject)
			pointsInCircle += localCount;
	}

	static void CalculateWithThread(int numThreads, int pointsPerThread)
	{
		pointsInCircle = 0;
		List<Thread> threads = new List<Thread>();
		DateTime start = DateTime.Now;

		for (int i = 0; i < numThreads; ++i)
		{
			Thread thread = new Thread(() => MonteCarloPi(pointsPerThread));
			threads.Add(thread);
			thread.Start();
		}

		foreach (Thread thread in threads)
			thread.Join();

		double elapsedTime = (DateTime.Now - start).TotalSeconds;
		double pi = 4.0 * pointsInCircle / (numThreads * pointsPerThread);

		Console.WriteLine("Przy użyciu Thread");
		Console.WriteLine("Wyliczona przybliżona wartość PI: " + pi);
		Console.WriteLine("Czas: " + elapsedTime + " sekund\n");
	}

	static void CalculateWithThreadPool(int numThreads, int pointsPerThread)
	{
		pointsInCircle = 0;
		CountdownEvent countdownEvent = new CountdownEvent(numThreads);
		DateTime start = DateTime.Now;

		for (int i = 0; i < numThreads; ++i)
		{
			ThreadPool.QueueUserWorkItem(_ =>
			{
				MonteCarloPi(pointsPerThread);
				countdownEvent.Signal();
			});
		}

		countdownEvent.Wait();
		double elapsedTime = (DateTime.Now - start).TotalSeconds;
		double pi = 4.0 * pointsInCircle / (numThreads * pointsPerThread);

		Console.WriteLine("Przy użyciu ThreadPool");
		Console.WriteLine("Wyliczona przybliżona wartość PI: " + pi);
		Console.WriteLine("Czas: " + elapsedTime + " sekund\n");
	}
}
