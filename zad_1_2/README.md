Polecenie:

Stosując klasę Random oraz jej metody NextDouble zaimplementować metodę numeryczną
Monte Carlo do obliczania liczby pi. Dla zwiększenia dokładności obliczeń uruchomić metodę w
wielu wątkach/zadaniach licząc liczbę pi jako średnią z tych wątków dla dwóch przypadków:
a) uruchamiając wątki/zadania z listy obiektów typu Thread
b) uruchamiając wątki/zadania z kolejki (puli) ThreadPool

Zadbać o właściwą synchronizację pomiędzy wątkami dodatkowymi oraz wątkami dodatkowymi i
wątkiem głównym. Zbadać, czy mechanizm przełączania wątków ma w tym zadaniu znaczący wpływ
na czas rozwiązania.

Thread:

- każde wywołanie wątku tworzy jego nowy obiekt,
- przy każdym tworzeniu obiektu, od nowa alokuje pamięć,
- po zakończeniu zadania wątek się kończy i jego zasoby są zwracane,
- pełna kontrola nad cyklem życia wątku (tworzenie, synchronizacja, zakończenie),
- kosztowne tworzenie i usuwanie wątków ze względu na alokację zasobów przez system operacyjny.

ThreadPool:

- wątki są tworzone raz przy inicjalizacji puli i są używane wielokrotnie do wykonania różnych zadań,
- pula wątków automatycznie zarządza cyklem życia wątków i dystrybucją ich zadań,
- programista definiuje tylko zadania do wykonania,
- mniejsze koszty tworzenia i usuwania wątków ze względu na ich reużywalność.

Thread jest lepszy do prostych, jednorazowych zadań, gdzie kontrola nad cyklem życia wątku jest kluczowa. Pula natomiast jest lepsza
do wykonywania wielu krótkich zadań ze względu na mniejsze koszty alokacji i zwalniania zasobów.

Realizacja zadania:

- wykorzystano lock, by zapewnić bezpieczeństwo i dostęp do nadpisywania zmiennej tylko dla jednego wątku jednocześnie,
- Environment.ProcessorCount zwraca dostępną liczbę wątków procesora w danym środowisku,
- Thread.join() wymusza na wątku głównym, żeby poczekał, aż metoda wykonywana przez wątek poboczny zakończy się wykonywać,
- w przypadku ThreadPool wykorzystano CountdownEvent, czyli licznik, który w konstruktorze przyjmuje liczbę zadań do wykonania.
  CountdownEvent.signal() zmniejsza licznik o 1, a CountdownEvent.Wait() blokuje dalsze wykonywanie się wątku, do momentu, aż licznik nie osiągnie wartość 0.
