Polecenie:
Napisać program przeszukujący tablicę wieloelementową (typ dowolny) dzieląc zakres poszukiwań
pomiędzy wątki/zadania. Liczba wątków powinna się równać liczbie wątków procesora. Porównać
czas wykonywania się tego programu z jego „wersją sekwencyjną”.

Uruchomienie:

- g++ -o zad_1_3 zad_1_3.cpp -std=c++11 -pthread
- ./zad_1_3

Atomic:

- typ danych odpowiedzialny za bezpieczną operację na zmiennych w kontekście wielowątkowym,
- operacje są wykonywane "atomowo", co oznacza, że operacja jest nieprzerwana i inne wątki nie mogą zobaczyć zmiennej w stanie pośrednim,
- chroni przed wyścigiem danych, gdy wątki modyfikują tą samą zmienną jednocześnie,
- nie wymaga przełączania kontekstu wątków, co zmniejsza koszty.

thread.join():

- metoda używana do synchronizacji wątków,
- gdy zostaje wykonywana na wątku t, to wątek główny (lub wywołujący) zostaje zablokowany do momentu aż wątek "t" zakończy swoje działanie,
- pozwala na uniknięcie sytuacji, w której wątek główny kończy swoje działanie przed wykonaniem zadania przez wątki dodatkowe,
- zasoby wewnątrz wątku bez wywołania join() lub detach() mogą nie zostać prawidłowo zwolnione, co może doprowadzić do ich wycieku,
- zapewnia, że operacje zależne od wyniku działania wątku, zostaną wykonane dopiero po zakończeniu jego pracy.
