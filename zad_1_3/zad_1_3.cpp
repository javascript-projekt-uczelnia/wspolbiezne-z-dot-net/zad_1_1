#include <iostream>
#include <thread>
#include <vector>
#include <chrono>
#include <atomic>

using namespace std;

void searchPart(const vector<int> &numbers, int start, int end, int x, atomic<int> &idx)
{
    for (int i = start; i < end; ++i)
    {
        if (idx != -1)
            return;

        if (numbers[i] == x)
        {
            idx = i;
            return;
        }
    }
}

double parallelSearch(const vector<int> &numbers, int x)
{
    cout << "Przeszukiwanie równoległe" << endl;
    unsigned int threadsCount = thread::hardware_concurrency();

    if (threadsCount == 0)
        threadsCount = 1;

    cout << "Szacowana liczba dostępnych wątków: " << threadsCount << endl;

    int chunkSize = numbers.size() / threadsCount;

    vector<thread> threads;
    atomic<int> idx(-1);

    auto start = chrono::high_resolution_clock::now();

    for (unsigned int i = 0; i < threadsCount; ++i)
    {
        int startIndex = i * chunkSize;
        int endIndex = (i == threadsCount - 1) ? numbers.size() : (i + 1) * chunkSize;

        threads.push_back(thread(searchPart, ref(numbers), startIndex, endIndex, x, ref(idx)));
    }

    for (auto &t : threads)
        t.join();

    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> elapsedSeconds = end - start;

    if (idx != -1)
        cout << "Znaleziono wartość: " << x << " pod indexem " << idx << endl;
    else
        cout << "Nie znaleziono wartości: " << x << endl;

    cout << "Czas przeszukiwania: " << elapsedSeconds.count() << " sekund\n\n";
    return elapsedSeconds.count();
}

double sequentialSearch(const vector<int> &numbers, int x)
{
    cout << "Przeszukiwanie sekwencyjne" << endl;
    auto start = chrono::high_resolution_clock::now();
    int idx = -1;

    for (size_t i = 0; i < numbers.size(); ++i)
    {
        if (numbers[i] == x)
        {
            idx = i;
            break;
        }
    }

    auto end = chrono::high_resolution_clock::now();
    chrono::duration<double> elapsedSeconds = end - start;

    if (idx != -1)
        cout << "Znaleziono wartość: " << x << " pod indexem " << idx << endl;
    else
        cout << "Nie znaleziono wartości: " << x << endl;

    cout << "Czas przeszukiwania: " << elapsedSeconds.count() << " sekund\n\n";
    return elapsedSeconds.count();
}

int main()
{
    cout << "Tablica zostanie wypełniona losowymi liczbami.\n";
    cout << "Następnie zostanie wylosowana wartość, której index będzie poszukiwany.\n";
    cout << "Podaj rozmiar tablicy: ";
    int numbers_count;
    cin >> numbers_count;

    vector<int> numbers(numbers_count);
    for (int i = 0; i < numbers_count; ++i)
        numbers[i] = rand() % 1000;

    int x = numbers[rand() % numbers_count];
    cout << "Wylosowana wartość do znalezienia: " << x << "\n\n";

    double sequential_time = sequentialSearch(numbers, x);
    double parallel_time = parallelSearch(numbers, x);

    return 0;
}
