Polecenie:
Napisać wersję równoległą algorytmu metody Monte Carlo z wykorzystaniem pętli równoległej klasy
Parallel (Parallel.For)

Wykonano zadanie:

- pętla Parallel dzieli zakres iteracji na mniejsze fragmenty, które będą wykonywane przez różne wątki, którymi sama zarządza,
- przyjmuje takie argumenty jak: fromInclusive - index początkowy włącznie, toExclusive - index końcowy wyłącznie, body - delegat wywołany raz na iterację https://learn.microsoft.com/pl-pl/dotnet/api/system.threading.tasks.parallel.for?view=net-8.0,
- wykorzystano lock, by upewnić się, że tylko jeden wątek może modyfikować współdzieloną wartość pointsInCircle i zapewnić jej atomowość (operacje są wykonywane jako jedna nieprzerwana jednostka pracy).
