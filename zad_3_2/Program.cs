﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

class Program
{
	static void Main(string[] args)
	{
		const string PI = "3.141592653589793238462643383279502884197";
		int pointsCount = 10000000;
		int pointsInCircle = 0;
		object lockObject = new object();

		Console.WriteLine("Wartość Pi: " + PI);
		Console.WriteLine("Wygenerowano punktów: " + pointsCount + "\n");

		Stopwatch stopwatch = new Stopwatch();
		stopwatch.Start();

		Parallel.For(0, pointsCount, () => new Random(), (i, loopState, random) =>
		{
			double x = random.NextDouble();
			double y = random.NextDouble();

			if (x * x + y * y <= 1.0)
			{
				lock (lockObject)
					pointsInCircle++;
			}

			return random;
		}, _ => { });

		stopwatch.Stop();

		double piEstimate = 4.0 * pointsInCircle / pointsCount;
		Console.WriteLine($"Wyliczona przybliżona wartość Pi: {piEstimate}");
		Console.WriteLine($"Czas pracy: {stopwatch.Elapsed.TotalSeconds} sekund");
	}
}
