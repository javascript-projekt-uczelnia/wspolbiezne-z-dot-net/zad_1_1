Rozwiązać zagadnienie „czytelników i pisarzy” za pomocą dedykowanej klasy ReaderWriterLockSlim i
jej bezparametrowymi metodami: EnterWriteLock(), ExitWriteLock(), EnterReadLock(),
ExitReadLock() – wszystkie one (parami) tworzą sekcje krytyczne dla bloków kodu (metod) tj.
odpowiednio metod(wątków) „pisarzy” i „czytelników”.

Założenia niefunkcjonalne:

- tablica z dowolnym typem elementów jako zmienna do zapisu/odczytu
- liczba wątków „pisarzy”: 4
- liczba wątków „czytelników”: 8
  -„pisarze” to metody uruchamiane w wątkach. Do metody „pisarza” przekazać dwa parametry
  reprezentujące: indeks w tablicy oraz nową wartość elementu z tym indeksem.
- „czytelnicy” to metody w wątkach z parametrem wejściowym w postaci indeksu elementu
  tablicy który ma być odczytany
- aby zwiększyć prawdopodobieństwo, że co najmniej dwa wątki z obydwu grup, lub tej samej
  grupy w tym samym czasie będą odnosić się do tablicy - utworzyć zmienne symulujące:
  - przerwy między odczytami i modyfikacjami tablicy
  - trwające odpowiednio długo fazy odczytu i zapisu
    Takie podejście pozwoli także na obserwację zmian w tablicy i ewentualną możliwość
    regulowania tymi parametrami w celu lepszej obserwacji
- aby poprawnie wykorzystać wspomnianą klasę i jej metody należy w metodach: modyfikującej i
  odczytującej tablicę zastosować wzorzec:
  obiekt.EnterWriteLock()
  try
  {
  //polecenia modyfikacji
  }
  Catch (Exception exc)
  {
  exc.Message
  }
  finally
  {
  obiekt.ExitWriteLock()
  }
  gdzie obiekt, to ten sam obiekt klasy RaederWriterLockSlim służący obydwu grupom wątków
- dodatkowa funkcjonalność: liczbę wątków oczekujących na zapis i odczyt można
  uzyskać za pomocą bezparametrowych metod:
  obiekt.WaitingWriteCount() , obiekt.WaitingReadCount()

Wykonanie zadania:

- utworzenie statycznej tablicy sharedArray, której elementy będą odczytywane i nadpisywane,
- pisarz otrzymuje jedynie indeks tablicy, gdyż wartość jest losowana,
- do zasymulowania przerw między zapisami i odczytami, a także procesu trwania zapisu i odczytu, zastosowano metodę Thread.sleep, przekazywany do niej czas jest losowany z zakresu od minBreakMs do maxBreakMs,

Klasa ReaderWriterLockSlim:

- klasa, która umożliwia jednoczesny dostęp do zasobu przez wiele wątków czytelników i zapewnia WYŁĄCZNY dostęp dla jednego wątku pisarza, co pozwala na częsty odczyt, a jednocześnie modyfikacje danych,
- EnterWriteLock() umożliwia wątkowi wejście do sekcji krytycznej jako pisarz (tylko jeden),
- ExitWriteLock() zwalnia dany wątek z sekcji krytycznej jako pisarza,
- EnterReadLock() umożliwia wątkowi wejście do sekcji krytycznej jako czytelnik, może ich być wiele,
- ExitReadLock() zwalnia dany wątek z sekcji krytycznej jako czytelnika,
- WaitingWriteCount - wyświetla liczbę wątków, oczekujących na zapis,
- WaitingReadCount - wyświetla liczbę wątków, oczekujących na odczyt.
