﻿using System;
using System.Threading;
using System.Threading.Tasks;

class Program
{
    static int minBreakMs = 100;
    static int maxBreakMs = 500;
    static int arrayLength = 8;
    static int writerCount = 4;
    static int readerCount = 8;

    static int[] sharedArray = new int[arrayLength];
    static ReaderWriterLockSlim rwLock = new ReaderWriterLockSlim();

    static void Main(string[] args)
    {
        for (int i = 0; i < writerCount; i++)
        {
            int writerId = i;
            Task.Run(() => Writer(writerId));
        }

        for (int i = 0; i < readerCount; i++)
        {
            int readerId = i;
            Task.Run(() => Reader(readerId));
        }

        Console.ReadLine();
    }

    static void Writer(int id)
    {
        Random random = new Random();
        while (true)
        {
            int index = random.Next(sharedArray.Length);
            int value = random.Next(100);

            Console.WriteLine($"Pisarz {id} czeka na dostęp do zapisu. Oczekujących pisarzy: {rwLock.WaitingWriteCount}, oczekujących czytelników: {rwLock.WaitingReadCount}");
            rwLock.EnterWriteLock();

            try
            {
                Console.WriteLine($"Pisarz {id} zapisuje wartość: {value} pod indexem: {index}");
                sharedArray[index] = value;
                Thread.Sleep(minBreakMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Pisarz {id} napotkał wyjątek: {ex.Message}");
            }
            finally
            {
                rwLock.ExitWriteLock();
            }

            Thread.Sleep(random.Next(minBreakMs, maxBreakMs));
        }
    }

    static void Reader(int id)
    {
        Random random = new Random();
        while (true)
        {
            int index = random.Next(sharedArray.Length);

            Console.WriteLine($"Czytelnik {id} czeka na dostęp do odczytu. Oczekujących pisarzy: {rwLock.WaitingWriteCount}, oczekujących czytelników: {rwLock.WaitingReadCount}");
            rwLock.EnterReadLock();

            try
            {
                int value = sharedArray[index];
                Console.WriteLine($"Czytelnik {id} odczytuje wartość: {value} pod indexem: {index}");
                Thread.Sleep(minBreakMs);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Czytelnik {id} napotkał wyjątek: {ex.Message}");
            }
            finally
            {
                rwLock.ExitReadLock();
            }

            Thread.Sleep(random.Next(minBreakMs, maxBreakMs));
        }
    }
}
