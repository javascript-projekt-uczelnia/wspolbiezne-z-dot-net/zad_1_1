Polecenie:
Napisać program umożliwiający współbieżny zapis (dopisywanie) do pliku dyskowego (tekstowego)
dowolnych treści generowanych w kilku metodach. Oprócz treści w pliku powinna znajdować się
informacja o czasie modyfikacji pliku tymi danymi.

Wykonanie:

- FileStream.WriteAsync(buffer, offset, count), gdzie buffer to dane, offset to przesunięcie bufferu, count to liczba bajtów do zapisania.
- FileStream.ReadAsync(buffer, offset, count)
