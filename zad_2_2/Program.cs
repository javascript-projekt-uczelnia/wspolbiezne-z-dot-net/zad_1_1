﻿using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

class Program
{
    static string filePath = "file.txt";
    static int numberLength = 50;
    static int taskCount = 1000;

    static async Task Main(string[] args)
    {
        File.WriteAllText(filePath, string.Empty);
        Task[] tasks = new Task[taskCount];

        for (int i = 0; i < taskCount; i++)
            tasks[i] = WriteToFileAsync();

        await Task.WhenAll(tasks);
        Console.WriteLine("Zapisano treści do pliku.\n");

        string fileContent = await ReadFileToEndAsync();
        Console.WriteLine("Zawartość pliku:");
        Console.WriteLine(fileContent);
    }

    static async Task WriteToFileAsync()
    {
        Random random = new Random();
		string currentTime = DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss.fff");
		string content = "";

        for (int i = 0; i < numberLength; i++)
            content += random.Next(0, 10);

        string formattedContent = $"[{currentTime}] {content}\n";
        byte[] encodedContent = Encoding.UTF8.GetBytes(formattedContent);

        using (FileStream fileStream = new FileStream(filePath, FileMode.Append, FileAccess.Write, FileShare.None, bufferSize: 4096, useAsync: true))
            await fileStream.WriteAsync(encodedContent, 0, encodedContent.Length);
    }

    static async Task<string> ReadFileToEndAsync()
    {
        string fileContent;

        using (FileStream fileStream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.Read, bufferSize: 4096, useAsync: true))
        {
            byte[] buffer = new byte[fileStream.Length];
            await fileStream.ReadAsync(buffer, 0, buffer.Length);
            fileContent = Encoding.UTF8.GetString(buffer);
        }

        return fileContent;
    }
}
