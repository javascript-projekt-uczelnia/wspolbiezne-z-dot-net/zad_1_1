Polecenie:
Napisać program demonstrujący/badający działanie kolejki/puli ThreadPool, wykorzystując metody
GetAvailableThreads oraz GetMaxThreads. Program powinien (w czasie działania kolejki) wyświetlać
liczbę wszystkich wątków wędrujących do kolejki, wątków dostępnych w kolejce oraz wątków
aktywnych (działających).

CPT - Completion Port Threads:

- wątki zarządzane przez pule wątków .NET,
- używane do obsługi wejścia/wyjścia asynchronicznego.

CountdownEvent:

- licznik, który służy do synchronizacji wielu wątków,
- signal() zmniejsza wartość licznika o jeden,
- wait() blokuje bieżący wątek do momentu, aż wartość licznika osiagnie zero.
