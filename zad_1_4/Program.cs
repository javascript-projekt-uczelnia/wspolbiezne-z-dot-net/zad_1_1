﻿using System;
using System.Threading;

class Program
{
	static void Main()
	{
		int workTimeMs = 3000;
		int checkTimeMs = 1000;

		int maxWorkerThreads, maxCompletionPortThreads;
		int availableWorkerThreads, availableCompletionPortThreads;

		ThreadPool.GetMaxThreads(out maxWorkerThreads, out maxCompletionPortThreads);

		Console.WriteLine("Etap: początek wykonywania programu.");
		Console.WriteLine("Maksymalna liczba wątków roboczych: {0}", maxWorkerThreads);
		Console.WriteLine("Maksymalna liczba wątków Completion Port: {0}\n", maxCompletionPortThreads);

		int tasksCount = 10;
		int checkCount = 0;

		CountdownEvent countdownEvent = new CountdownEvent(tasksCount);

		for (int i = 0; i < tasksCount; i++)
		{
			int taskNum = i;
			ThreadPool.QueueUserWorkItem(state =>
			{
				Console.WriteLine("Rozpoczęto wykonywanie zadania nr. {0}.", taskNum);
				Thread.Sleep(workTimeMs);
				Console.WriteLine("Zakończono wykonywanie zadania nr. {0}.", taskNum);
				countdownEvent.Signal();
			});
		}

		while (!countdownEvent.Wait(checkTimeMs))
		{
			ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableCompletionPortThreads);

			Console.WriteLine("\nEtap: oczekiwanie na wykonanie zadań nr. {0}", checkCount++);
			Console.WriteLine("Dostępne wątki robocze: {0}", availableWorkerThreads);
			Console.WriteLine("Dostępne wątki Completion Port: {0}", availableCompletionPortThreads);
			Console.WriteLine("Aktywne wątki robocze: {0}\n", maxWorkerThreads - availableWorkerThreads);
		}

		countdownEvent.Wait();

		ThreadPool.GetAvailableThreads(out availableWorkerThreads, out availableCompletionPortThreads);
		Console.WriteLine("\nEtap: zakończono wykonywanie zadań.");
		Console.WriteLine("Dostępne wątki robocze: {0}", availableWorkerThreads);
		Console.WriteLine("Dostępne wątki Completion Port: {0}", availableCompletionPortThreads);
		Console.WriteLine("Aktywne wątki robocze: {0}", maxWorkerThreads - availableWorkerThreads);
	}
}
