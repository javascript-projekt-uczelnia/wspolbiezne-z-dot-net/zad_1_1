Polecenie:
Napisz program sprawdzający współbieżnie/równolegle aktualny status kilku usług (serwerów).
Do wysyłania żądań http (get) i badania odpowiedzi na te żądania zastosować klasę HttpClient i jej
metodę GetAsync. Co oznacza asynchroniczne wykonywanie się metody? Program powinien wypisać
informacje o numerach wątków przydzielonych do badania poszczególnych witryn oraz o
zakończeniu programu.

Asynchroniczne wykonywanie się metody oznacza, że metoda nie zatrzymuje działania całej aplikacji w oczekiwaniu
na jej rezultat. W czasie oczekiwania na jej wykonanie, zasoby mogą zostać wykorzystane na inne zadania. Dlatego
wykorzystując mechanizm asynchroniczności, można równolegle wykonać wiele zadań. Jest to szczególnie przydatne w przypadku,
gdy pobieramy dane z serwera (np. plik hmtl lub odpowiedź w formacie json).

Lock sprawia, że tylko jeden wątek może w danym czasie zmodyfikować wybraną zmienną.
