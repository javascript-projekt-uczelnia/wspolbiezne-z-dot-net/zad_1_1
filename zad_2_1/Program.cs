﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;

class Program
{
	private static int successfulResponsesCount = 0;
	private static readonly object lockObject = new object();

	static async Task Main(string[] args)
	{
		List<string> urls = new List<string>
		{
			"https://www.google.com",
			"https://gothicmultiplayerteam.gitlab.io/docs/0.3.0/",
			"https://pl.wikipedia.org/",
			"https://gitlab.com/",
			"https://tutaj_nie_ma_odpowiedzi.com/"
		};

		HttpClient httpClient = new HttpClient();
		List<Task> tasks = new List<Task>();

		foreach (var url in urls)
			tasks.Add(CheckUrlStatus(httpClient, url));

		await Task.WhenAll(tasks);
		Console.WriteLine("\nZakończono sprawdzanie wszystkich usług.");
		Console.WriteLine($"Pozytywnych odpowiedzi było {successfulResponsesCount} na {urls.Count}.");
	}

	static async Task CheckUrlStatus(HttpClient httpClient, string url)
	{
		int threadId = Thread.CurrentThread.ManagedThreadId;
		Console.WriteLine($"Rozpoczęto sprawdzanie {url} na wątku {threadId}\n");

		try
		{
			HttpResponseMessage response = await httpClient.GetAsync(url);
			string status = response.IsSuccessStatusCode ? "dostępna" : "niedostępna";

			if (response.IsSuccessStatusCode)
			{
				lock (lockObject)
					successfulResponsesCount++;
			}

			Console.WriteLine($"Usługa {url} jest {status}. Status code: {response.StatusCode}\n");
		}
		catch (HttpRequestException e)
		{
			Console.WriteLine($"Błąd podczas sprawdzania {url}: {e.Message}");
		}

		Console.WriteLine($"Zakończono sprawdzanie {url} na wątku {threadId}");
	}
}
