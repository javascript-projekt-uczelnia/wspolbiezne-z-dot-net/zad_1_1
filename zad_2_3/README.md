Polecenie: Napisać program symulujący dwa konta bankowe o unikalnych identyfikatorach, z możliwością
typowych operacji dla salda, w tym także równoczesnego dokonywania przelewów pomiędzy tymi
kontami tj. z konta 1 na 2 (bądź odwrotnie).

Wykonanie:

- w celu uniknięcia zakleszczenia (sytuacji, w której oba wątki są zakleszczone i czekają na siebie nawzajem) wykorzystano identyfikatory kont, przez co można
  na ich podstawie zdecydować, które konto należy zablokować najpierw, w wypadku tego zadania to o mniejszym id.
