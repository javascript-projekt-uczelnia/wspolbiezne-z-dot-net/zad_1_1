﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

class Program
{
	static async Task Main(string[] args)
	{
		Account account1 = new Account(1, 1000);
		Account account2 = new Account(2, 2000);

		Console.WriteLine($"Saldo konta 1: {account1.Balance}");
		Console.WriteLine($"Saldo konta 2: {account2.Balance}\n");

		List<Task> tasks = new List<Task>
		{
			Task.Run(() => Transfer(account1, account2, 250)),
			Task.Run(() => Transfer(account2, account1, 300)),
			Task.Run(() => Transfer(account1, account2, 600)),
			Task.Run(() => Transfer(account2, account1, 300)),
		};

		await Task.WhenAll(tasks);
		Console.WriteLine($"\nSaldo konta 1: {account1.Balance}");
		Console.WriteLine($"Saldo konta 2: {account2.Balance}");
	}

	static void Transfer(Account fromAccount, Account toAccount, decimal amount)
	{
		if (fromAccount.Id < toAccount.Id)
		{
			lock (fromAccount)
			{
				lock (toAccount)
				{
					if (fromAccount.Balance >= amount)
					{
						fromAccount.Withdraw(amount);
						toAccount.Deposit(amount);
						Console.WriteLine($"Przelew {amount} z konta {fromAccount.Id} na konto {toAccount.Id}.");
					}
				}
			}
		}
		else
		{
			lock (toAccount)
			{
				lock (fromAccount)
				{
					if (fromAccount.Balance >= amount)
					{
						fromAccount.Withdraw(amount);
						toAccount.Deposit(amount);
						Console.WriteLine($"Przelew {amount} z konta {fromAccount.Id} na konto {toAccount.Id}.");
					}
				}
			}
		}
	}
}

public class Account
{
	public int Id { get; }
	private decimal balance;
	public decimal Balance
	{
		get
		{
			lock (this)
				return balance;
		}
	}

	public Account(int id, decimal initialBalance)
	{
		Id = id;
		balance = initialBalance;
	}

	public void Deposit(decimal amount)
	{
		lock (this)
			balance += amount;
	}

	public void Withdraw(decimal amount)
	{
		lock (this)
			balance -= amount;
	}
}
