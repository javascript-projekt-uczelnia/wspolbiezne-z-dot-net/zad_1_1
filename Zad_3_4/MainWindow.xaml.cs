﻿using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using ZAD_3_4.Models;

namespace ZAD_3_4
{
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void GenerateProducts_Click(object sender, RoutedEventArgs e)
		{
			progressBar.IsIndeterminate = true;

			Task.Run(() =>
			{
				using (var context = new AdventureWorksContext())
				{
					for (int i = 0; i < 10000; i++)
					{
						var product = new Product { Name = $"Product {i + 1}" };
						context.Products.Add(product);
					}

					context.SaveChanges();
				}

				Dispatcher.Invoke(() =>
				{
					progressBar.IsIndeterminate = false;
					MessageBox.Show("Dodano nowe produkty do bazy danych.", "Success", MessageBoxButton.OK, MessageBoxImage.Information);
				});
			});
		}
		private void ShowAllProducts_Click(object sender, RoutedEventArgs e)
		{
			progressBar.IsIndeterminate = true;

			Task.Run(() =>
			{
				DataTable dataTable = new DataTable();

				using (var context = new AdventureWorksContext())
				{
					var query = from product in context.Products
								select product;

					var products = query.ToList();

					Dispatcher.Invoke(() =>
					{
						dataTable.Columns.Add("ProductID");
						dataTable.Columns.Add("Name");

						foreach (var product in products)
						{
							dataTable.Rows.Add(product.ProductID, product.Name);
						}

						dataGrid.ItemsSource = dataTable.DefaultView;
						progressBar.IsIndeterminate = false;
					});
				}
			});
		}

		private async void ExecuteQuery_Click(object sender, RoutedEventArgs e)
		{
			progressBar.IsIndeterminate = true;

			DataTable dataTable = await Task.Run(() =>
			{
				using (var context = new AdventureWorksContext())
				{
					var query = from prod in context.Products
								join th in context.TransactionHistories on prod.ProductID equals th.ProductID
								join tha in context.TransactionHistoryArchives on th.ProductID equals tha.ProductID
								where tha.TransactionDate > new DateTime(2011, 3, 1)
								group new { th, tha } by new { prod.ProductID, prod.Name } into g
								select new
								{
									g.Key.ProductID,
									g.Key.Name,
									TotalCost = g.Sum(x => x.th.ActualCost * x.th.Quantity + x.tha.ActualCost * x.tha.Quantity)
								};

					var result = query.OrderByDescending(x => x.TotalCost).Take(10).ToList();

					DataTable dt = new DataTable();
					dt.Columns.Add("ProductID");
					dt.Columns.Add("Name");
					dt.Columns.Add("TotalCost");

					foreach (var item in result)
					{
						dt.Rows.Add(item.ProductID, item.Name, item.TotalCost);
					}

					return dt;
				}
			});

			dataGrid.ItemsSource = dataTable.DefaultView;
			progressBar.IsIndeterminate = false;
		}
	}
}
