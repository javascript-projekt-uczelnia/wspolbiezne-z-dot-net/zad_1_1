﻿using System.Data.Entity;
using ZAD_3_4.Models;

namespace ZAD_3_4
{
	public class AdventureWorksContext : DbContext
	{
		public AdventureWorksContext()
			: base("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename='C:\\Marek\\Studia\\semestr 6\\Współbieżne\\Zadania\\Zad_1_1\\Zad_3_4\\AdventureWorksLocal.mdf';Integrated Security=True")
		{
		}

		public DbSet<Product> Products { get; set; }
		public DbSet<TransactionHistory> TransactionHistories { get; set; }
		public DbSet<TransactionHistoryArchive> TransactionHistoryArchives { get; set; }

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
		}
	}
}
