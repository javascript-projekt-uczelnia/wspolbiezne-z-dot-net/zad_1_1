use AdventureWorks2017
select
    top 10 prod.ProductID,
    prod.Name,
    SUM(
        th.ActualCost * th.Quantity + tha.ActualCost * tha.Quantity
    )
from
    Production.TransactionHistory th
    INNER JOIN Production.TransactionHistoryArchive tha ON th.ProductID = tha.ProductID
    INNER JOIN Production.Product prod ON prod.ProductID = th.ProductID
where
    tha.TransactionDate > '2011-03-01'
group by
    prod.ProductID,
    prod.Name
order by
    SUM(
        th.ActualCost * th.Quantity + tha.ActualCost * tha.Quantity
    ) desc

CREATE TABLE Product (
    ProductID int PRIMARY KEY,
    Name nvarchar(255)
);

CREATE TABLE TransactionHistory (
    TransactionID int PRIMARY KEY,
    ProductID int,
    ActualCost decimal(10, 2),
    Quantity int,
    CONSTRAINT FK_Product_TransactionHistory FOREIGN KEY (ProductID) REFERENCES Product(ProductID)
);

CREATE TABLE TransactionHistoryArchive (
    TransactionID int PRIMARY KEY,
    ProductID int,
    ActualCost decimal(10, 2),
    Quantity int,
    TransactionDate date,
    CONSTRAINT FK_Product_TransactionHistoryArchive FOREIGN KEY (ProductID) REFERENCES Product(ProductID)
);
