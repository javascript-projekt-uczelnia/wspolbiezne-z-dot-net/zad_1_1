﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using ZAD_3_4.Models;

namespace ZAD_3_4
{
	public class AdventureWorksInitializer : DropCreateDatabaseIfModelChanges<AdventureWorksContext>
	{
		protected override void Seed(AdventureWorksContext context)
		{
			var random = new Random();
			var products = new List<Product>();

			for (int i = 1; i <= 1000; i++)
			{
				products.Add(new Product { ProductID = i, Name = $"Product{i}" });
			}

			products.ForEach(p => context.Products.Add(p));
			context.SaveChanges();

			var transactionHistories = new List<TransactionHistory>();
			var transactionHistoryArchives = new List<TransactionHistoryArchive>();

			for (int i = 1; i <= 1000; i++)
			{
				for (int j = 0; j < 10; j++)
				{
					transactionHistories.Add(new TransactionHistory
					{
						ProductID = i,
						ActualCost = random.Next(50, 200),
						Quantity = random.Next(1, 20),
						TransactionDate = DateTime.Now.AddDays(-random.Next(0, 365))
					});

					transactionHistoryArchives.Add(new TransactionHistoryArchive
					{
						ProductID = i,
						ActualCost = random.Next(50, 200),
						Quantity = random.Next(1, 20),
						TransactionDate = DateTime.Now.AddDays(-random.Next(366, 730))
					});
				}
			}

			transactionHistories.ForEach(th => context.TransactionHistories.Add(th));
			transactionHistoryArchives.ForEach(tha => context.TransactionHistoryArchives.Add(tha));
			context.SaveChanges();
		}
	}
}
