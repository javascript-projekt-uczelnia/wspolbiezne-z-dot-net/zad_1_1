﻿namespace ZAD_3_4.Models
{
    public class Product
    {
        public int ProductID { get; set; }
        public string Name { get; set; }
    }
}
