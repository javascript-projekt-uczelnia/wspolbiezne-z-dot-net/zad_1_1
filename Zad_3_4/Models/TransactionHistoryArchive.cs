﻿namespace ZAD_3_4.Models
{
	public class TransactionHistoryArchive
	{
		public int TransactionHistoryArchiveID { get; set; }
		public int ProductID { get; set; }
		public decimal ActualCost { get; set; }
		public int Quantity { get; set; }
		public DateTime TransactionDate { get; set; }

		public virtual Product Product { get; set; }
	}
}
