﻿using System.Data.Entity;
using System.Windows;

namespace ZAD_3_4
{
	public partial class App : Application
	{
		protected override void OnStartup(StartupEventArgs e)
		{
			Database.SetInitializer(new AdventureWorksInitializer());
			base.OnStartup(e);
		}
	}
}
