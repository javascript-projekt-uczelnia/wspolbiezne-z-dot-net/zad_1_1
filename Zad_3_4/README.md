Polecenie:
Korzystając z gotowego zapytania zapytanie.sql napisać responsywną aplikację WPF realizującą
powyższe zapytanie do bazy danych, dodatkowo dołączyć pasek postępu dla tego zapytania.

Dispatcher jest to obiekt w środowisku WPF, który umożliwia dostęp do elementów interfejsu użytkownika z wątku interfejsu użytkownika (UI thread) z innych wątków.
W kontekście aplikacji WPF, UI thread jest wątkiem, który obsługuje wszystkie operacje interfejsu użytkownika, takie jak rysowanie elementów interfejsu użytkownika, obsługa zdarzeń interakcji z użytkownikiem itp.