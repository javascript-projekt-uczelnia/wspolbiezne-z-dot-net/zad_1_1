﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media.Imaging;
using LiveCharts;
using LiveCharts.Wpf;

namespace Zad_3_3
{
	public partial class MainWindow : Window
	{
		private const int _width = 800;
		private const int _height = 600;
		private const int msDelay = 50;
		public ChartValues<double> LineData { get; set; }

		public MainWindow()
		{
			InitializeComponent();
			LineData = new ChartValues<double>();
			DataContext = this;
		}

		private void Przygotuj_Click(object sender, RoutedEventArgs e)
		{
			var img = new System.Drawing.Bitmap(_width, _height);
			for (int i = 0; i < _width; i++)
			{
				for (int j = 0; j < _height; j++)
				{
					double cos = Math.Cos((i + j) * Math.PI / 180.0);
					byte val = Convert.ToByte(255.0 * Math.Abs(cos));
					img.SetPixel(i, j, System.Drawing.Color.FromArgb(val, val, val));
				}
			}
			ImagePreview.Source = BitmapToImageSource(img);
		}

		private async void Analizuj_Click(object sender, RoutedEventArgs e)
		{
			if (ImagePreview.Source is BitmapSource bitmapSource)
			{
				var orgBitmap = BitmapSourceToBitmap(bitmapSource);

				await Task.Run(() =>
				{
					double[] lineData = new double[orgBitmap.Width];

					for (int i = 0; i < orgBitmap.Height; i++)
					{
						var tempBitmap = new System.Drawing.Bitmap(orgBitmap);
						for (int j = 0; j < orgBitmap.Width; j++)
						{
							lineData[j] = tempBitmap.GetPixel(j, i).R;
						}
						Dispatcher.Invoke(() =>
						{
							UpdateChart(lineData);
							ImagePreview.Source = BitmapToImageSource(tempBitmap);
						});
						Thread.Sleep(msDelay);
					}
				});
			}
		}

		private void UpdateChart(double[] yvalues)
		{
			LineData.Clear();
			foreach (var value in yvalues)
			{
				LineData.Add(value);
			}
		}

		private System.Drawing.Bitmap BitmapSourceToBitmap(BitmapSource bitmapSource)
		{
			var bitmap = new System.Drawing.Bitmap(bitmapSource.PixelWidth, bitmapSource.PixelHeight, PixelFormat.Format32bppArgb);
			var data = bitmap.LockBits(new System.Drawing.Rectangle(System.Drawing.Point.Empty, bitmap.Size), ImageLockMode.WriteOnly, PixelFormat.Format32bppArgb);
			bitmapSource.CopyPixels(Int32Rect.Empty, data.Scan0, data.Height * data.Stride, data.Stride);
			bitmap.UnlockBits(data);
			return bitmap;
		}

		private BitmapImage BitmapToImageSource(System.Drawing.Bitmap bitmap)
		{
			using (MemoryStream memory = new MemoryStream())
			{
				bitmap.Save(memory, ImageFormat.Bmp);
				memory.Position = 0;
				BitmapImage bitmapImage = new BitmapImage();
				bitmapImage.BeginInit();
				bitmapImage.StreamSource = memory;
				bitmapImage.CacheOption = BitmapCacheOption.OnLoad;
				bitmapImage.EndInit();
				return bitmapImage;
			}
		}
	}
}
