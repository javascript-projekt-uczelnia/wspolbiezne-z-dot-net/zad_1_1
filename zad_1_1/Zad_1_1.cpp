﻿#include <iostream>
#include <mpi.h>
#include <random>
#include <vector>

using namespace std;

const int root_rank = 0;

void MPI_BCast_example(int rank, int size)
{
    int data_size = 8;
    int data[data_size] = {0};

    if (rank == root_rank)
    {
        cout << "Root inicjalizuje tablice\n";
        for (int i = 0; i < data_size; ++i)
            data[i] = i * size;
    }

    MPI_Bcast(data, data_size, MPI_INT, root_rank, MPI_COMM_WORLD);

    if (rank != root_rank)
    {
        cout << "Proces " << rank << " otrzymał dane od roota: ";
        for (int i = 0; i < data_size; ++i)
        {
            cout << data[i] << (i == data_size - 1 ? "\n" : ", ");
        }
    }
}

void MPI_Send_Receive_example(int rank, int size)
{
    int tagValue = 0;

    // Parzysty proces wysyła, nieparzysty proces odbiera.
    if (rank % 2 == 0)
    {
        int next_rank = rank + 1;
        if (next_rank < size)
        {
            random_device rd;
            mt19937 gen(rd());

            uniform_int_distribution<> dis(0, 100);
            int random_number = dis(gen);

            cout << "Proces " << rank << " wysyła liczbę " << random_number << " do procesu " << next_rank << endl;

            // https://cpp0x.pl/dokumentacja/mpich/MPI_Send/122
            // int MPI_Send( void * msg, int count, MPI_Datatype datatype, int dest, int tag, MPI_Comm comm );
            MPI_Send(&random_number, 1, MPI_INT, next_rank, tagValue, MPI_COMM_WORLD);
        }
    }
    else
    {
        int previous_number = rank - 1;
        int received_number;

        MPI_Recv(&received_number, 1, MPI_INT, previous_number, tagValue, MPI_COMM_WORLD, MPI_SUCCESS);
        cout << "Proces " << rank << " otrzymał liczbę " << received_number << " od procesu " << previous_number << endl;
    }
}

void MPI_Scatter_Gather_example(int rank, int size)
{
    const int data_size = size * 2;
    const int data_per_rank_count = 2;

    vector<int> send_data(data_size);

    if (rank == root_rank)
    {
        for (int i = 0; i < data_size; ++i)
        {
            send_data[i] = i;
        }
    }

    vector<int> recv_data(data_per_rank_count);

    // https://www.mpich.org/static/docs/latest/www3/MPI_Scatter.html
    // const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm
    MPI_Scatter(send_data.data(), data_per_rank_count, MPI_INT, recv_data.data(), data_per_rank_count, MPI_INT, root_rank, MPI_COMM_WORLD);

    cout << "Proces " << rank << " otrzymał dane:\n";
    for (int i = 0; i < data_per_rank_count; ++i)
    {
        int newValue = recv_data[i] + rank;
        cout << recv_data[i] << " i zmienił na: " << newValue << endl;
        recv_data[i] += rank;
    }
    cout << endl;

    vector<int> recv_buffer(data_size);

    // https://www.mpich.org/static/docs/latest/www3/MPI_Gather.html
    // const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, int root, MPI_Comm comm
    MPI_Gather(recv_data.data(), data_per_rank_count, MPI_INT, recv_buffer.data(), data_per_rank_count, MPI_INT, 0, MPI_COMM_WORLD);

    if (rank == root_rank)
    {
        cout << "Zgromadzone dane z procesów: ";
        for (int i = 0; i < data_size; ++i)
            cout << recv_buffer[i] << (i == data_size - 1 ? "\n" : ", ");
    }
}

void MPI_Scatter_Allgather_example(int rank, int size)
{
    int data_count_per_rank = 2;
    int data_size = size * data_count_per_rank;

    vector<int> send_data(data_size);

    if (rank == root_rank)
    {
        for (int i = 0; i < data_size; ++i)
            send_data[i] = i;
    }

    vector<int> recv_data(data_size);
    
    MPI_Scatter(send_data.data(), data_count_per_rank, MPI_INT, recv_data.data(), data_count_per_rank, MPI_INT, root_rank, MPI_COMM_WORLD);

    // https://www.mpich.org/static/docs/latest/www3/MPI_Allgather.html
    // const void *sendbuf, int sendcount, MPI_Datatype sendtype, void *recvbuf, int recvcount, MPI_Datatype recvtype, MPI_Comm comm

    MPI_Allgather(recv_data.data(), data_count_per_rank, MPI_INT, recv_data.data(), data_count_per_rank, MPI_INT, MPI_COMM_WORLD);
    cout << "Proces " << rank << " otrzymał dane:\n";
    for (int i = 0; i < data_size; ++i)
        cout << recv_data[i] << (i == data_size - 1 ? "\n\n" : ", ");
}

int main(int argc, char **argv)
{
    MPI_Init(&argc, &argv);

    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    // MPI_BCast_example(rank, size);
    // MPI_Send_Receive_example(rank, size);
    // MPI_Scatter_Gather_example(rank, size);
    MPI_Scatter_Allgather_example(rank, size);

    MPI_Finalize();
    return 0;
}
