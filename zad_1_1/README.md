Polecenie:

1. Korzystając z dokumentacji OPEN MPI zapoznać się z funkcjami:

- MPI_BCAST
- MPI_SEND
- MPI_RECEIVE
- MPI_SCATTER
- MPI_GATHER
- MPI_ALLGATHER
  Napisać swoje własne programy ilustrujące działanie powyższych funkcji.

OPEN MPI - implementacja standardu Message Passing Interface (MPI), która
umożliwia komunikację między procesami w rozproszonych środowiskach obliczeniowych.
Realizuje programy w klastrach i superkomputerach.

Działanie poszczególnych funkcji:

- MPI_BCAST - rozsyła dane z procesu root'a do wszystkich pozostałych procesów,
- MPI_SEND - wysyła dane do konkretnego procesu,
- MPI_RECEIVE - odbiera dane od określonego procesu,
- MPI_SCATTER - rozsyła dane z jednego procesu do wszystkich procesów w komunikatorze, rozdzielając je na mniejsze porcje pomiędzy procesami,
- MPI_GATHER - zbiera rozdzielone dane od wszystkich procesów w komunikatorze i gromadzi je w JEDNYM procesie,
- MPI_ALLGATHER - zbiera dane od wszystkich procesóww i rozsyła z powrotem do wszystkich procesów, przez co każdy proces ma dostęp do danych każdego innego procesu.

Instalacja OPEN MPI:

- sudo apt install openmpi-bin openmpi-common libopenmpi-dev
- mpic++ Zad_1_1.cpp -o Zad_1_1
- mpirun -np 10 ./Zad_1_1

Inne funkcje:

- MPI_Init - inicjalizuje MPI,
- MPI_Finalize - finalizuje MPI i zwalnia zajęte zasoby,
- MPI_Comm_rank - pobiera identyfikator danego procesu i nadpisuje nim podaną zmienną,
- MPI_Comm_size - pobiera liczbę utworzonych procesów i nadpisuje nią podaną zmienną.
